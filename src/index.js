import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';

import Diagram from './Diagram';
import store from './store';

import { bla } from './flowTest';

ReactDOM.render(
  <Provider store={store}>
    <Diagram />
  </Provider>,
  document.getElementById('root')
);

