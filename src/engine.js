import {
  DiagramEngine,
  DefaultLinkFactory,
  DiagramModel,
  DefaultNodeFactory,
  DefaultNodeModel,
  DefaultPortModel,
  DiagramWidget,
  LinkModel
} from "storm-react-diagrams";


const engine = new DiagramEngine();

engine.registerNodeFactory(new DefaultNodeFactory());
engine.registerLinkFactory(new DefaultLinkFactory());

export default engine;