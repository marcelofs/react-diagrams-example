import { createStore } from 'redux';

const initialState = {

};

// function newNode

function reducer(state = initialState, action) {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1
    case 'DECREMENT':
      return state - 1
    default:
      return state
  }
}

const store = createStore(reducer);
export default store;