import React, { Component } from 'react';

import {
  DiagramModel,
  DefaultNodeModel,
  DefaultPortModel,
  DiagramWidget,
  LinkModel
} from "storm-react-diagrams";

import 'storm-react-diagrams/dist/style.css';

import engine from './engine';

class Diagram extends Component {

  constructor() {
    super();
    const model = new DiagramModel();

    const node1 = new DefaultNodeModel("Node 1","rgb(0,192,255)");
    const port1 = node1.addPort(new DefaultPortModel(false,"out-1","Out"));
    node1.x = 100;
    node1.y = 100;

    const node2 = new DefaultNodeModel("Node 2","rgb(192,255,0)");
    const port2 = node2.addPort(new DefaultPortModel(true,"in-1", 'In'));
    node2.x = 400;
    node2.y = 100;

    const node3 = new DefaultNodeModel("Node 3","rgb(192,255,0)");
    const port3 = node3.addPort(new DefaultPortModel(true,"in-3", 'In'));
    const port3b = node3.addPort(new DefaultPortModel(false,"out-3", 'Out'));
    node3.x = 400;
    node3.y = 400;

    const link1 = new LinkModel();
    link1.setSourcePort(port1);
    link1.setTargetPort(port2);

    model.addNode(node1);
    model.addNode(node2);
    model.addNode(node3);
    model.addLink(link1);
    this.engine = engine;
    this.engine.setDiagramModel(model);
  }

  render() {
    return (
      <div style={{ display: 'flex', width: '100%', height: '100%' }}>
        <DiagramWidget diagramEngine={this.engine} />
      </div>
    );
  }
}

export default Diagram;
